extern crate clap; use clap::{Arg, App};
extern crate spidev; use spidev::{Spidev, SpidevOptions};
use std::io;
use std::io::prelude::*;


fn main() {
    let options = App::new("pl9823 - command-line utility for controlling PL9823 LEDs.")
                          .version("Version 1.0")
                          .about("Uses MOSI pin of SPI port. On raspi you may need to add 'core_freq=250' to your /boot/config.txt.\npl9823 expects space-separated list of comma-separated color values on input. For example:\n\techo '255,0,0 0,255,0 0,0,255' | ./pl9823\n")
                          .arg(Arg::with_name("device").short("d").long("device").value_name("SPI_DEVICE_PATH").help("set SPI device to use").default_value("/dev/spidev0.0").takes_value(true))
                          .arg(Arg::with_name("frequency").short("f").long("frequency").value_name("HZ").help("set SPI frequency (58000000 for RPI0, 7300000 for RPI4)").default_value("5800000").takes_value(true))
                          .arg(Arg::with_name("t0byte").short("0").long("t0byte").value_name("0-255").help("set SPI byte for T0").default_value("192").takes_value(true))
                          .arg(Arg::with_name("t1byte").short("1").long("t1byte").value_name("0-255").help("set SPI byte for T1").default_value("240").takes_value(true))
                          .arg(Arg::with_name("reset").short("r").long("reset").value_name("INTEGER").help("set length of reset sequence in SPI bytes").default_value("50").takes_value(true))
                          .get_matches();


    let mut spi = Spidev::open(options.value_of("device").unwrap()).expect("Can't open SPI device");
    spi.configure(&SpidevOptions::new().max_speed_hz(options.value_of("frequency").unwrap().parse::<u32>().unwrap()).bits_per_word(8).build()).expect("Can't configure SPI device");
    let t0byte: u8 = options.value_of("t0byte").unwrap().parse().unwrap();
    let t1byte: u8 = options.value_of("t1byte").unwrap().parse().unwrap();
    let reset_length: u32 = options.value_of("reset").unwrap().parse().unwrap();
    loop {
        let mut input = String::new();
        match io::stdin().read_line(&mut input) {
            Ok(0) => { break; } //EOF
            Ok(_) => {
                let mut bytes_vector = Vec::new();
                for pixel in input.split_whitespace() {
                    for color in pixel.split(",") {
                        let color_value: u8 = color.parse().unwrap();
                        for bit_i in 0..8 {
                            bytes_vector.push(if (1 << (7-bit_i)) & color_value == 0 { t0byte } else { t1byte });
                        }
                    }
                }
                for _ in 0..reset_length { bytes_vector.push(0); };
                spi.write(&bytes_vector).expect("Can't write to SPI device");
            }
            Err(error) => { println!("Error reading from stdin: {}", error); break; }
        }
    }
}
